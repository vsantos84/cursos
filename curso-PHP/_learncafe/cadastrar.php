<?php
header("Content-Type: text/html;  charset=UTF-8",true);
include_once "conexao.php";

try {

	$nome = filter_var($_POST['nNome']);
	$login = filter_var($_POST['nLogin']);
	
	$insert = $conectar->prepare("INSERT INTO login (nome, login) VALUES (:nome, :login)");
	$insert->bindParam(':nome', $nome);
	$insert->bindParam(':login',$login);
	$insert->execute();
	header("location: index.php");
	
} catch (PDOException $e) {

	echo "Erro: " . $e->getmessage();
}
?>