<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="_css/estilo.css"/>
  <meta charset="UTF-8"/>
  <title>Curso de PHP - CursoemVideo.com</title>
  <style>
  	h1 {
  		font: 12pt Arial;
  		color: #273747;
  	}
  </style>
</head>
<body>
<div>
	<?php
    	echo "<h1>exercicio _1_ </h1>";
        
		$valor = $_POST["v"];
		
		echo "<br/>o valor enviado foi $valor";
		
		echo "<br/>a raiz quadrada é " . number_format(sqrt($valor),2);
		echo "<br/><a href='aula08.html'>voltar</a>";
		
    	echo "<br/><br/>"."..."."<br/><br/>";
    ?>
    
    <?php
    	echo "<h1>exercicio _2_ </h1>";
        
		$nome = isset($_POST["n"])?$_POST["n"]:"não informado";
		$ano = isset($_POST["a"])?$_POST["a"]:0;
		$sexo = isset($_POST["s"])?$_POST["s"]:"[sem sexo]";
		
		echo "<br/>pessoa $nome";
		echo "<br/>ano de nascimento $ano";
		echo "<br/>Sexo " . ($sexo==1?"masculino":"feminino");
		
		echo "<br/>sua idade atual é " . (date("Y") - $ano);
		
		echo "<br/><a href='aula08.html'>voltar</a>";
		
    	echo "<br/><br/>"."..."."<br/><br/>";
    ?>
    
</div>
</body>
</html>
 