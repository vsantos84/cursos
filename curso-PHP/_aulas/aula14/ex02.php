<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="_css/estilo.css"/>
  <meta charset="UTF-8"/>
  <title>Curso de PHP - CursoemVideo.com</title>
  <style>
  	h1 {
  		font: 12pt Arial;
  		color: #273747;
  	}
  </style>
</head>
<body>
<div>
	<?php
    	echo "<h1>exercicio _2_ </h1>";
        
		function soma() {
			$p = func_get_args();
			$tot = func_num_args();
			$s=0;
			for($i=0; $i<$tot; $i++){
				$s += $p[$i];
			}
			return $s;
		}
		
		echo "<br/>a soma vale ".soma(3,4,5);
		
    	echo "<br/><br/>"."..."."<br/><br/>";
		echo "<br/><a href='ex01.html'>voltar</a>";
    ?>
</div>
</body>
</html>
 