<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="_css/estilo.css"/>
  <meta charset="UTF-8"/>
  <title>Curso de PHP - CursoemVideo.com</title>
  <style>
  	h1 {
  		font: 12pt Arial;
  		color: #273747;
  	}
  </style>
</head>
<body>
<div>
	<?php
    	echo "<h1>exercicio _1_ </h1>";
        
		$prod = "leite";
		$preco = 4.5;
		
		echo "<br/>O $prod custa R$ ".number_format($preco, 2);
		printf("<br/>O %s custa R$ %.2f", $prod ,$preco);
		echo "<br/>";
		
		$x[0] = 4;
		$x[1] = 14;
		$x[2] = 24;
		$x[3] = 34;
		
		print_r($x);
		
		
		echo "<br/><br/>";
		$v = array (1,2,5,7,9,5);
		print_r($v);
		
		echo "<br/><br/>";
		$v = array (1,2,5,7,9,5);
		var_dump($v);
		
		echo "<br/><br/>";
		$v = array (1,2,5,7,9,5);
		var_export($v);
		
		echo "<br/><br/>";
		$v = "array (1,2,5,7,9,5) akdk akldjak fdakjfdlkaj fdafjadskljfadlkçfda  fdakjfdafjçldaflkdajkdfla fdalfkjd";
		echo "$v";
		echo "<br/><br/>";
		$s = wordwrap($v, 20, "<br/>\n", true);
		echo ($s);
		
    	echo "<br/><br/>"."..."."<br/><br/>";
		echo "<br/><a href='aula.html'>voltar</a>";
		
    ?>
</div>
</body>
</html>