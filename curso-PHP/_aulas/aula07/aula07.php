<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="_css/estilo.css"/>
  <meta charset="UTF-8"/>
  <title>Curso de PHP - CursoemVideo.com</title>
  <style>
  	h1 {
  		font: 12pt Arial;
  		color: #273747;
  	}
  </style>
</head>
<body>
<div>
    <?php
    	echo "<h1>exercicio _1_ </h1>";
        
		$n1 = $_GET["a"];
		$n2 = $_GET["b"];
		$tipo = $_GET["tipo"];
		
		$maior = ( $tipo == "s" )? $n1+$n2: $n1*$n2 ;
		echo "<br/>Os valores foram $n1 e $n2 e a soma é " . $maior;
		
    	echo "<br/><br/>"."..."."<br/><br/>";
    ?>
    
     <?php
    	echo "<h1>exercicio _2_ </h1>";
        
		$a = 3;
		$b = "3";
		$r = ( $a == $b )? "sim": "não" ;
		echo "<br/>as variaveis são iguais " . $r;
		
		$r = ( $a === $b )? "sim": "não" ;
		echo "<br/>as variaveis são identicas " . $r;
		
    	echo "<br/><br/>"."..."."<br/><br/>";
    ?>
    
     <?php
    	echo "<h1>exercicio _3_ </h1>";
        
		$nota1 = $_GET["n1"];
		$nota2 = $_GET["n2"];
		$media = ($nota1+$nota2)/2 ;
		
		echo "<br/>Os valores foram $nota1 e $nota2";
		echo "<br/>A média é " . $media;
		
		$sit = ($media>6)?"aprovado":"reprovado";
		echo "<br/>A situação é " . $sit;
		echo "<br/>A situação é " . (($media>6)?"aprovado":"reprovado");
		
    	echo "<br/><br/>"."..."."<br/><br/>";
    ?>
    
    <?php
    	echo "<h1>exercicio _4_ </h1>";
        
		$idade = 2016 - $_GET["i"];
		echo "<br/>A idade é  $idade";
		echo "<br/>A média é " . $media;
		
		$sit = ($idade>18 && $idade <66)?"obrigatório":"opcional";
		echo "<br/>Votar é " . $sit;
		
    	echo "<br/><br/>"."..."."<br/><br/>";
    ?>
</div>
</body>
</html>
 