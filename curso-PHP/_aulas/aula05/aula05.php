<html>
<head>
  <link rel="stylesheet" href="../_css/estilo.css"/>
  <meta charset="UTF-8"/>
  <style>
  	h1 {
  		font: 12pt Arial;
  		color: #273747;
  	}
  	
  </style>
  <title>Curso de PHP - CursoemVideo.com</title>
</head>
<body>
<div>
    <?php
    	$n1 = $_GET["a"];
		$n2 = $_GET["b"];
		$v1 = $n1;
		$v2 = $n2;
		
    	echo "<h2>Valores recebidos: $v1 e $v2</h2>";
	?>
</div>
<div>
    <?php
    	echo "<h1>Operações aritmeticas</h1>";
		$soma = $n1+$n2;
		$sub = $n1-$n2;
		$vezes = $n1*$n2;
		$div = $n1/$n2;
		$resto = $n1%$n2;
		
		echo "A soma vale $soma<br/>";
		echo "A subtração vale $sub<br/>";
		echo "A multiplicação vale $vezes<br/>";
		echo "A divisão vale $div<br/>";
		echo "O resto vale $resto<br/>";
	?>
</div>
<div>
	<?php
    	echo "<h1>Operações aritmeticas</h1>";
		
		echo "O valor absoluto de -$v2 e " . abs(-$v2);
        echo "<br/>O valor de $v1<sup>$v2</sup> e " . pow($v1, $v2);
		
        echo "<br/>A raiz de $v1 e " . sqrt($v1);
        echo "<br/>O valor de $v2 arredondado e " . round($v2); // ceil floor
        echo "<br/>A parte inteira de $v2 e " . intval($v2);
        echo "<br/>O valor de $v1 em moeda e R$" . number_format($v1,2,",", ".");
    ?>
</div>
</body>
</html>