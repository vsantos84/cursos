<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="_css/estilo.css"/>
  <meta charset="UTF-8"/>
  <title>Curso de PHP - CursoemVideo.com</title>
  <style>
  	h1 {
  		font: 12pt Arial;
  		color: #273747;
  	}
  </style>
</head>
<body>
<div>
	<?php 
		echo "<h1>exercicio 1</h1>";
        $preco = $_GET["p"];
		echo "O preço do produto é $preco<br/>";
		$preco += $preco*10/100; 
		echo "o novo preço com 10% de aumento será $ " . number_format($preco,2);
		echo "<br/><br/>"."..."."<br/><br/>";
    ?>
    
    <?php 
    	echo "<h1>exercicio 2</h1>";
        $ano = $_GET["ano"];
		echo "O ano atual é $ano<br/>";  // teste1
		echo "o novo ano é " . ++$ano . "<br/>"; #teste 2
    	echo "<br/><br/>"."..."."<br/><br/>";
    ?>
    
    <?php #exercicio 3
    	echo "<h1>exercicio 3</h1>";
        $a = 3;
		$b = 4;
		echo "$a $b<br/>";
		$b = &$a;
		echo "$a $b<br/>";
		echo "<br/><br/>"."..."."<br/><br/>";
    ?>
    
    <?php #exercicio 4
    	echo "<h1>exercicio 4</h1>";
        $a = "cursoemvideo";
		$$a = "cursophp";
		echo $a ."<br/>";
		echo $cursoemvideo."<br/>";
    	echo "<br/><br/>"."..."."<br/><br/>";
    ?>
    
    
</div>
</body>
</html>
 