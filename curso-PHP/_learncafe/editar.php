<?php
header("Content-Type: text/html;  charset=UTF-8",true);
include_once "conexao.php";

try {
	$id = filter_var($_POST['nId'], FILTER_SANITIZE_NUMBER_INT);
	$nome = filter_var($_POST['nNome']);
	$login = filter_var($_POST['nLogin']);
	
	$update = $conectar->prepare("UPDATE login SET nome = :nome, login = :login WHERE id = :id");
	$update->bindParam(':id', $id);
	$update->bindParam(':nome', $nome);
	$update->bindParam(':login',$login);
	$update->execute();
	header("location: index.php");
	
} catch (PDOException $e) {

	echo "Erro: " . $e->getmessage();
}
?>